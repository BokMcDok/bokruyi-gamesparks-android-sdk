package com.example.test1;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CheckBox;

import com.gamesparks.sdk.*;
import com.gamesparks.sdk.android.*;
import com.gamesparks.sdk.api.autogen.*;
import com.gamesparks.sdk.api.autogen.GSRequestBuilder.*;
import com.gamesparks.sdk.api.autogen.GSResponseBuilder.*;
import com.gamesparks.sdk.api.GSData;

public class MainActivity extends ActionBarActivity
{
	private enum Servers {
		PRODUCTION,
		STAGE,
		TEST
	}

	private boolean		mConnected = false;
	private boolean		mLiveMode = true;
	private Context		mContext;
	private EditText 	mEditText;
	private TextView 	mTextView1;
	private TextView	mTextView2;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	
		setContentView(R.layout.activity_main);

		mEditText = (EditText)findViewById(R.id.editText1);
		mTextView1 = (TextView)findViewById(R.id.textView1);
		mTextView2 = (TextView)findViewById(R.id.textView2);

		mContext = this;

		Button button1 = (Button)findViewById(R.id.button1);
		button1.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				connect();
			}
		});

		Button button2 = (Button)findViewById(R.id.button2);
		button2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				disconnect();
			}
		});

		Button button3 = (Button)findViewById(R.id.button3);
		button3.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				mEditText.setText("");
			}
		});

		Button button4 = (Button)findViewById(R.id.button4);
		button4.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				authenticate();
			}
		});

		Button button5 = (Button)findViewById(R.id.button5);
		button5.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				sendLogEventRequest();
			}
		});

		Button button6 = (Button)findViewById(R.id.button6);
		button6.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				sendEndSessionRequest();
			}
		});

		initGS();
	}

	public void refreshEditText(String line) {
		String finalText;

		finalText = line + "\n" + mEditText.getText().toString();

		mEditText.setText(finalText);
	}

	@Override
	public void onStart() {
		super.onStart();

		connect();
	}

	@Override
	public void onStop() {
		super.onStop();

		disconnect();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	private void initGS() {
		GSAndroidPlatform.initialise(mContext, "2974660weiMa", "p5pFVnohi5eWPYETb4aPgeMLtd95bjfJ", "", mLiveMode, true);

		GSAndroidPlatform.gs().setOnAvailable(new GSEventConsumer<Boolean>() {
			@Override
			public void onEvent(Boolean available) {
			if (available) {
				mConnected = true;

				mTextView1.setText("Connected");

				refreshEditText("OK");
			} else {
				mConnected = false;

				mTextView1.setText("Not connected");
				mTextView2.setText(" ");

				refreshEditText("ERROR!");
			}
			}
		});

		GSAndroidPlatform.gs().setOnAuthenticated(new GSEventConsumer<String>() {
			@Override
			public void onEvent(String s) {
			if (s != null && !s.equals("") && GSAndroidPlatform.gs().isAuthenticated()) {
				mTextView2.setText("Authenticated");
			} else {
				mTextView2.setText(" ");
			}
			}
		});
	}

	private void connect()
	{
		if (!mConnected) {
			mTextView1.setText("Connecting...");

			refreshEditText("Connecting...");

			GSAndroidPlatform.gs().start();
		}
	}

	private void disconnect()
	{
		if (mConnected) {
			mConnected = false;

			GSAndroidPlatform.gs().stop();

			mTextView1.setText("Disconnected");

			if (!GSAndroidPlatform.gs().isAuthenticated()) {
				mTextView2.setText(" ");
			}

			refreshEditText("Disconnected");
		}
	}

	private void authenticate()
	{
		if (mConnected)
		{
			mTextView1.setText("Authenticating...");

			refreshEditText("Authenticating...");

			GSAndroidPlatform.gs().getRequestBuilder().createDeviceAuthenticationRequest()
					.setDeviceId("12345")
					.setDeviceOS("MacOS")
					.send(new GSEventConsumer<AuthenticationResponse>() {
						@Override
						public void onEvent(AuthenticationResponse event) {
							if (event.getAuthToken() == null || event.getAuthToken().equals(""))
							{
								mTextView1.setText("Connected");

								refreshEditText("ERROR!");
							}
							else
							{
								mTextView1.setText("Ready");

								refreshEditText("OK");
							}
						}
					});
		}
	}

	private void sendLogEventRequest()
	{
		if (mConnected)
		{
			mTextView1.setText("Sending LogEventRequest...");

			refreshEditText("Sending LogEventRequest...");

			LogEventRequest request = GSAndroidPlatform.gs().getRequestBuilder().createLogEventRequest()
					.setEventKey("001")
					.setEventAttribute("0001", "AAAH");

			//request.setDurable(true);
			request.send(new GSEventConsumer<LogEventResponse>() {
				@Override
				public void onEvent(LogEventResponse logEventResponse) {
					GSData scriptData = logEventResponse.getScriptData();

					if (GSAndroidPlatform.gs().isAuthenticated()) {
						mTextView1.setText("Ready");
					}
					else
					{
						mTextView1.setText("Connected");
					}
					if (logEventResponse.hasErrors()) {
						refreshEditText("ERROR!");
					}
					else {
						refreshEditText("OK");
					}
				}
			});
		}
	}

	private void sendEndSessionRequest() {
		if (mConnected) {
			mTextView1.setText("Sending EndSessionRequest...");

			refreshEditText("Sending EndSessionRequest...");

			EndSessionRequest request = GSAndroidPlatform.gs().getRequestBuilder().createEndSessionRequest();

			request.send(new GSEventConsumer<EndSessionResponse>() {
				@Override
				public void onEvent(EndSessionResponse endSessionResponse) {
					if (endSessionResponse.hasErrors()) {
						refreshEditText("ERROR!");
					}
					else {
						refreshEditText("OK");
					}
				}
			});
		}
	}
}
