/**
 * 
 */
package com.gamesparks.sdk.android;

import java.io.File;
import java.util.List;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import com.gamesparks.sdk.GS;
import com.gamesparks.sdk.GSEventConsumer;
import com.gamesparks.sdk.IGSPlatform;
import com.gamesparks.sdk.api.GSData;
import com.gamesparks.sdk.api.autogen.GSMessageHandler;
import com.gamesparks.sdk.api.autogen.GSMessageHandler.ScriptMessage;
import com.gamesparks.sdk.api.autogen.GSResponseBuilder.AroundMeLeaderboardResponse;
import com.gamesparks.sdk.api.autogen.GSResponseBuilder.AuthenticationResponse;
import com.gamesparks.sdk.api.autogen.GSTypes.LeaderboardData;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
//import android.org.apache.commons.codec.binary.Base64;
import android.util.*;
import android.os.Bundle;
import android.os.Handler;

/**
 * @author Giuseppe Perniola
 *
 */
public class GSAndroidPlatform implements IGSPlatform
{
	private static final String GS_CURR_STATUS = "GSCurrStatus";

	private static GS 	gs;
	private Handler 	mainHandler;
	private Context 	ctx;

	public static GS initialise(final Context ctx, String apiKey, String secret, String credential, boolean liveMode, boolean autoUpdate)
	{
		if (gs == null)
		{
			GSAndroidPlatform gsAndroidPlatform = new GSAndroidPlatform(ctx);

			if (!liveMode)
			{
				Intent intent = new Intent(ctx, GSActivity.class);

				ctx.startActivity(intent);
			}

			gs = new GS(apiKey, secret, credential, liveMode, autoUpdate, gsAndroidPlatform);
		}

		return gs;
	}
	
	private GSAndroidPlatform(Context ctx)
	{
		this.ctx = ctx;
		
		mainHandler = new Handler(ctx.getMainLooper());
	}
	
	public static GS gs()
	{
		return gs;
	}
		
	/* (non-Javadoc)
	 * @see com.gamesparks.sdk.IGSPlatform#getWritableLocation()
	 */
	@Override
	public File getWritableLocation()
	{
		return ctx.getFilesDir();
	}
	
	public void storeValue(String key, String value)
	{
		try
		{
			SharedPreferences 			settings = ctx.getSharedPreferences(GS_CURR_STATUS, Context.MODE_PRIVATE);
			SharedPreferences.Editor 	editor = settings.edit();
			
			editor.putString(key, value);

			editor.commit();
		}
		catch (Exception e)
		{
			
		}
	}
	
	public String loadValue(String key)
	{   
		try
		{
			SharedPreferences settings = ctx.getSharedPreferences(GS_CURR_STATUS, Context.MODE_PRIVATE);
			
			return settings.getString(key, "");
		}
		catch (Exception e)
		{
			return "";
		}
	}

	/* (non-Javadoc)
	 * @see com.gamesparks.sdk.IGSPlatform#executeOnMainThread(java.lang.Runnable)
	 */
	@Override
	public void executeOnMainThread(Runnable job)
	{	
		if (mainHandler != null)
		{
			mainHandler.post(job);
		}
	}

	/* (non-Javadoc)
	 * @see com.gamesparks.sdk.IGSPlatform#getPlayerId()
	 */
	@Override
	public String getPlayerId()
	{
		return loadValue("playerId");
	}

	/* (non-Javadoc)
	 * @see com.gamesparks.sdk.IGSPlatform#getAuthToken()
	 */
	@Override
	public String getAuthToken()
	{
		return loadValue("authToken");
	}

	/* (non-Javadoc)
	 * @see com.gamesparks.sdk.IGSPlatform#setPlayerId()
	 */
	@Override
	public void setPlayerId(String value)
	{
		storeValue("playerId", value);
	}

	/* (non-Javadoc)
	 * @see com.gamesparks.sdk.IGSPlatform#setAuthToken()
	 */
	@Override
	public void setAuthToken(String value)
	{
		storeValue("authToken", value);
	}
	
	@Override
	public Object getHmac(String nonce, String secret)
	{
		try
		{
			Mac 			sha256_HMAC = Mac.getInstance("HmacSHA256");
			SecretKeySpec 	secret_key = new SecretKeySpec(secret.getBytes("UTF-8"), "HmacSHA256");

			sha256_HMAC.init(secret_key);

			//return Base64.encodeBase64String(sha256_HMAC.doFinal(nonce.getBytes("UTF-8")));
			return Base64.encodeToString(sha256_HMAC.doFinal(nonce.getBytes("UTF-8")), Base64.NO_WRAP);
		}
		catch (Exception e)
		{
			return null;
		}
	}

	@Override
	public void logMessage(String msg)
	{
		System.out.println(msg);
	}

	@Override
	public void logError(Throwable t)
	{
		System.out.println(t.getMessage());
	}

}
